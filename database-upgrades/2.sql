﻿DROP TABLE IF EXISTS metric_interestrate_irswap_fixedfloat;
DROP TABLE IF EXISTS metric_interestrate_irswap_ois;
DROP TABLE IF EXISTS metric_interestrate_irswap_basis;
DROP TABLE IF EXISTS metric_interestrate_crosscurrency_basis;
DROP TABLE IF EXISTS metric_interestrate_crosscurrency_fixedfloat;
DROP TABLE IF EXISTS metric_interestrate_crosscurrency_fixedfixed;
DROP TABLE IF EXISTS metric_interestrate_option_swaption;
DROP TABLE IF EXISTS metric_interestrate_capfloor;

CREATE TABLE metric_interestrate_irswap_fixedfloat (
    dissemination_id bigint PRIMARY KEY,
    execution_timestamp bigint,
    settlement_currency varchar,
    effective_date bigint,
    end_date bigint,
    underlying_asset_1 varchar,
    underlying_asset_2 varchar,
    rounded_notional_amount_1 bigint,
    rounded_notional_overflow_flag_1 boolean,
    reset_frequency_1 varchar,
    reset_frequency_2 varchar,
    cleared varchar,
    indication_of_collateralization varchar,
    indication_of_end_user_exception varchar,
    execution_venue varchar,
    common_fixed_fair_rate double precision,
    end_date_fixed_fair_rate double precision,
    effective_date_fixed_fair_rate double precision,
    spread_delta varchar,
    fixed_delta varchar,
    flat_delta varchar
);

CREATE TABLE metric_interestrate_irswap_ois (
    dissemination_id bigint PRIMARY KEY,
    execution_timestamp bigint,
    settlement_currency varchar,
    effective_date bigint,
    end_date bigint,
    rounded_notional_amount_1 bigint,
    rounded_notional_overflow_flag_1 boolean,
    cleared varchar,
    indication_of_collateralization varchar,
    indication_of_end_user_exception varchar,
    execution_venue varchar,
    common_fixed_fair_rate double precision,
    end_date_fixed_fair_rate double precision,
    effective_date_fixed_fair_rate double precision,
    spread_delta varchar,
    fixed_delta varchar,
    flat_delta varchar
);

CREATE TABLE metric_interestrate_irswap_basis (LIKE metric_interestrate_irswap_ois INCLUDING ALL);

CREATE TABLE metric_interestrate_crosscurrency_basis (
    dissemination_id bigint PRIMARY KEY,
    execution_timestamp bigint,
    notional_currency_1 varchar,
    notional_currency_2 varchar,
    effective_date bigint,
    end_date bigint,
    rounded_notional_amount_1 bigint,
    rounded_notional_overflow_flag_1 boolean,
    rounded_notional_amount_2 bigint,
    rounded_notional_overflow_flag_2 boolean,
    cleared varchar,
    indication_of_collateralization varchar,
    indication_of_end_user_exception varchar,
    execution_venue varchar,
    common_fixed_fair_rate double precision,
    end_date_fixed_fair_rate double precision,
    effective_date_fixed_fair_rate double precision,
    spread_delta varchar,
    fixed_delta varchar,
    xccy_delta varchar,
    flat_delta varchar
);

CREATE TABLE metric_interestrate_crosscurrency_fixedfloat (LIKE metric_interestrate_crosscurrency_basis INCLUDING ALL);
CREATE TABLE metric_interestrate_crosscurrency_fixedfixed (LIKE metric_interestrate_crosscurrency_basis INCLUDING ALL);

CREATE TABLE metric_interestrate_option_swaption (
    dissemination_id bigint PRIMARY KEY,
    execution_timestamp bigint,
    settlement_currency varchar,
    option_expiration_date bigint,
    effective_date bigint,
    end_date bigint,
    rounded_notional_amount_1 bigint,
    rounded_notional_overflow_flag_1 boolean,
    option_type varchar,
    option_strike_price real,
    underlying_asset_1 varchar,
    underlying_asset_2 varchar,
    reset_frequency_1 varchar,
    reset_frequency_2 varchar,
    cleared varchar,
    indication_of_collateralization varchar,
    indication_of_end_user_exception varchar,
    execution_venue varchar,
    spread_delta varchar,
    fixed_delta varchar,
    vega_matrix varchar,
    volatility double precision,
    flat_delta varchar
);

CREATE TABLE metric_interestrate_capfloor (LIKE metric_interestrate_option_swaption INCLUDING ALL);

CREATE INDEX metric_interestrate_irswap_fixedfloat_effective_date ON metric_interestrate_irswap_fixedfloat(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_irswap_ois_effective_date ON metric_interestrate_irswap_ois(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_irswap_basis_effective_date ON metric_interestrate_irswap_basis(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_crosscurrency_basis_effective_date ON metric_interestrate_crosscurrency_basis(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_crosscurrency_fixedfloat_effective_date ON metric_interestrate_crosscurrency_fixedfloat(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_crosscurrency_fixedfixed_effective_date ON metric_interestrate_crosscurrency_fixedfixed(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_option_swaption_effective_date ON metric_interestrate_option_swaption(effective_date DESC NULLS LAST);
CREATE INDEX metric_interestrate_capfloor_effective_date ON metric_interestrate_capfloor(effective_date DESC NULLS LAST);

GRANT SELECT ON TABLE metric_interestrate_irswap_fixedfloat TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_irswap_ois TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_irswap_basis TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_crosscurrency_basis TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_crosscurrency_fixedfloat TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_crosscurrency_fixedfixed TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_option_swaption TO GROUP ro_user;
GRANT SELECT ON TABLE metric_interestrate_capfloor TO GROUP ro_user;

UPDATE config SET value = '2'
WHERE key = 'version';