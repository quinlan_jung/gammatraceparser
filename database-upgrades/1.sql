﻿DROP TABLE IF EXISTS interest_swap;
CREATE TABLE interest_swap (
    dissemination_id bigint PRIMARY KEY,
    original_dissemination_id varchar,
    action varchar,
    execution_timestamp bigint,
    cleared varchar,
    indication_of_collateralization varchar,
    indication_of_end_user_exception varchar,
    indication_of_other_price_affecting_term varchar,
    block_trades_and_large_notional_off_facility_swaps varchar,
    execution_venue varchar,
    effective_date bigint,
    end_date bigint,
    day_count_convention varchar,
    settlement_currency varchar,
    asset_class varchar,
    sub_asset_class_for_other_commodity varchar,
    taxonomy varchar,
    price_forming_continuation_data varchar,
    underlying_asset_1 varchar,
    underlying_asset_2 varchar,
    price_notation_type varchar,
    price_notation real,
    additional_price_notation_type varchar,
    additional_price_notation real,
    notional_currency_1 varchar,
    notional_currency_2 varchar,
    rounded_notional_amount_1 bigint,
    rounded_notional_overflow_flag_1 boolean,
    rounded_notional_amount_2 bigint,
    rounded_notional_overflow_flag_2 boolean,
    payment_frequency_1 varchar,
    payment_frequency_2 varchar,
    reset_frequency_1 varchar,
    reset_frequency_2 varchar,
    embeded_option varchar,
    option_strike_price real,
    option_type varchar,
    option_family varchar,
    option_currency varchar,
    option_premium real,
    option_lock_period varchar,
    option_expiration_date bigint,
    price_notation2_type varchar,
    price_notation2 real,
    price_notation3_type varchar,
    price_notation3 real
);

CREATE INDEX interest_swap_effective_date ON interest_swap(effective_date DESC NULLS LAST);

GRANT SELECT ON TABLE interest_swap TO GROUP ro_user;

UPDATE config SET value = '1'
WHERE key = 'version';