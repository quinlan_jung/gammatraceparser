DROP TABLE IF EXISTS config;
CREATE TABLE config(
    key    varchar(50) PRIMARY KEY,
    value  varchar(50)
);

GRANT SELECT ON TABLE config TO GROUP ro_user;

INSERT INTO config (key, value) VALUES ('archiverWatermark', '0');

INSERT INTO config (key, value) VALUES ('version', '0');