/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.config;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gammatrace.database.DatabaseClient;
import com.gammatrace.helpers.SqlUtils;
import com.gammatrace.parser.DBOrchestrator;

@Configuration
public class DatabaseConfig {	
	@Bean
	public DatabaseClient databaseClient() throws ClassNotFoundException, SQLException, PropertyVetoException{
		return new DatabaseClient("rw");
	}
	
	@Bean
	public SqlUtils sqlUtils(DatabaseClient databaseClient){
		return new SqlUtils(databaseClient);
	}
	
	@Bean
	public DBOrchestrator dBOrchestrator(SqlUtils sqlUtils){
		return new DBOrchestrator(sqlUtils);
	}

}
