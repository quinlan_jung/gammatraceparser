/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.gammatrace.stats.CloudwatchClient;

@Configuration
public class MetricConfig {
	String namespace = "Gammatrace Pollers";

	@Bean
	public CloudwatchClient cloudwatchClient(){
		String cloudwatch_id = AppConfig.getProperty("cloudwatch-id");
		String cloudwatch_secret = AppConfig.getProperty("cloudwatch-secret");
		AWSCredentials awsCredentials = new BasicAWSCredentials(cloudwatch_id, cloudwatch_secret);
		Region region = Region.getRegion(Regions.valueOf(AppConfig.getProperty("cloudwatch-region")));
		return new CloudwatchClient(awsCredentials, region, namespace);
	}
}
