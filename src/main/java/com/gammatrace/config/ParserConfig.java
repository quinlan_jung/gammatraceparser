/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gammatrace.database.DatabaseClient;
import com.gammatrace.handler.ArchiveHandler;
import com.gammatrace.handler.Handler;
import com.gammatrace.helpers.WatermarkHelper;
import com.gammatrace.parser.DBOrchestrator;
import com.gammatrace.poller.Poller;
import com.gammatrace.stats.CloudwatchClient;

@Configuration
public class ParserConfig {
	private static final int POLL_INTERVAL_SECONDS = 30;
	private static final String archiveWatermarkKey = "archiverWatermark";
	private String parseUrl = AppConfig.getProperty("dtccParseUrl");
	
	@Bean(initMethod = "start")
	public Poller archivePoller(Handler archiveHandler, CloudwatchClient cloudwatchClient){
		return new Poller (archiveHandler, POLL_INTERVAL_SECONDS, cloudwatchClient);
	}
	
	@Bean
	public Handler archiveHandler(DBOrchestrator dBOrchestrator, DatabaseClient databaseClient){
		WatermarkHelper watermarkHelper = new WatermarkHelper(databaseClient, archiveWatermarkKey);
		return new ArchiveHandler(watermarkHelper, dBOrchestrator, parseUrl);
	}

}
