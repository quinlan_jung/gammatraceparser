/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.gammatrace.logging.CustomLogger;
import com.google.common.base.Joiner;

public class AppConfig {
	static Logger logger = CustomLogger.getLogger(AppConfig.class);
	static Properties prop = new Properties();
	static Path configDir = Paths.get("./config");
	static String globalSymbol = "*";
	static String delimiter = ".";
	static String domain;
	
	public static void init() throws IOException{
		List<Path> configFiles = listFiles(configDir);
		
		for (Path file : configFiles){
			InputStream inputStream = Files.newInputStream(file);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("Property file '" + file + "' not found in the classpath");
			}
		}
 
		// get the property value and print it out
		domain = prop.getProperty("domain");
		logger.info("Detected domain: " + domain);
	}
	
	private static List<Path> listFiles(Path path) throws IOException {
		final List<Path> files = new ArrayList<Path>();
	    try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
	        for (Path entry : stream) {
	            if (Files.isDirectory(entry)) {
	                files.addAll(listFiles(entry));
	            }

	            else if (Files.isRegularFile(entry) && !entry.getFileName().startsWith(".")){
	            	files.add(entry);
	            }
	        }
	    }
	    return files;
	}
	
	public static String getProperty(String property){
		String [] domainArray = {domain, property};
		String [] globalArray = {globalSymbol, property};
		String domainKey = Joiner.on(delimiter).join(domainArray);
		String globalKey = Joiner.on(delimiter).join(globalArray);
		return (prop.getProperty(domainKey) == null) ? prop.getProperty(globalKey) : prop.getProperty(domainKey);
	}
}
