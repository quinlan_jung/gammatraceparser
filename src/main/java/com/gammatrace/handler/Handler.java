/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.handler;

public interface Handler {
	public void handle() throws Exception;
	
	public String getName();
}
