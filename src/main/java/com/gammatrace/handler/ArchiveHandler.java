/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.handler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.helpers.TableLogic;
import com.gammatrace.helpers.WatermarkHelper;
import com.gammatrace.logging.CustomLogger;
import com.gammatrace.parser.DBOrchestrator;
import com.gammatrace.parser.DTCCParser;
import com.gammatrace.parser.Sorter;

public class ArchiveHandler implements Handler {
	Logger logger = CustomLogger.getLogger(ArchiveHandler.class);
	WatermarkHelper watermarkHelper;
	String parseUrl;
	DBOrchestrator dBOrchestrator;
	
	public ArchiveHandler(WatermarkHelper watermarkHelper, DBOrchestrator dBOrchestrator, String parseUrl){
		this.parseUrl = parseUrl;
		this.dBOrchestrator = dBOrchestrator;
		this.watermarkHelper = watermarkHelper;
	}

	@Override
	public void handle() throws IOException, XMLStreamException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		DateTime watermark = watermarkHelper.getWatermarkFromDatabase();
		logger.info(String.format("Retrieved watermark from database %s", watermark.toString()));
		
		Sorter sorter = new Sorter();
		DTCCParser dtccParser = new DTCCParser(parseUrl, watermark, sorter);
		DateTime feedPubDate = dtccParser.parsePage();
		logger.info(String.format("Parsed DTCC RSS feed with publication date of %s", feedPubDate.toString()));

		if (watermark.isBefore(feedPubDate)) {
			dBOrchestrator.modifyFromDisk(TableLogic.getArchivalTable(), TableLogic.getArchivalColumnsAsString(), sorter.getInsertRows(), DtccValue.DISSEMINATION_ID.toString(), sorter.getDeleteRowIds());
			
			logger.info(String.format("Updating database watermark with date of %s", feedPubDate.toString()));
			watermarkHelper.updateWatermark(feedPubDate);
		}
		
		logger.info("Parsing handler complete.");
	}

	@Override
	public String getName() {
		return "ArchiveHandler";
	}
}
