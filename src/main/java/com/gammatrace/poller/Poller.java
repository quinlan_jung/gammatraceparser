/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.poller;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;

import com.gammatrace.commonutils.TimeUtils;
import com.gammatrace.handler.Handler;
import com.gammatrace.logging.CustomLogger;
import com.gammatrace.stats.CloudwatchClient;

//TODO: Make a nanny process to resurrect Main in case the JVM gets killed

/**
 * Calls handler AT LEAST once each polling interval
 *
 */
public class Poller extends Thread{
	Logger logger = CustomLogger.getLogger(Poller.class);
	long runCompleted;
	private Handler handler;
	private int pollInterval;
	private CloudwatchClient cloudwatchClient;
	private String successMetricName;
	
	public Poller (Handler handler, int pollInterval, CloudwatchClient cloudwatchClient){
		this.handler = handler;
		this.pollInterval = pollInterval;
		this.cloudwatchClient = cloudwatchClient;
		this.successMetricName = handler.getName() + "Success";
	}
	
	private void sleepForAwhile() throws InterruptedException{
		long currentRun = getRun();
		
		// We are not yet due for a run
		if (currentRun == runCompleted){
			long sleepTime = (currentRun + pollInterval) - getTimeNow();
			logger.info(String.format("%s : Too early to do a run, sleeping for: %d", handler.getName(), sleepTime));
			Thread.sleep(sleepTime * DateTimeConstants.MILLIS_PER_SECOND);
		}
		
		runCompleted = getRun();
	}
	
	private long getTimeNow (){
		DateTime dt = new DateTime(DateTimeZone.UTC);
		return dt.getMillis() / DateTimeConstants.MILLIS_PER_SECOND;
	}
	
	private long getRun (){
		long timeNow = getTimeNow();
		return timeNow - (timeNow % pollInterval); // now, rounded down to the nearest 30 seconds
	}
	
	@Override
	public void run() {
		try {
			logger.info(String.format("%s : Starting up poller for ", handler.getName()));
			runCompleted = getRun();
			while (true){
				sleepForAwhile();
				logger.info(String.format("%s : Processing run: %s", handler.getName(), TimeUtils.toDateTime(runCompleted).toString()));
				handler.handle();
				cloudwatchClient.postMetricSuccess(successMetricName);
			}
		} catch (Throwable e) {
			logger.error(String.format("%s : Poller encountered an error.", handler.getName()), e);	
			cloudwatchClient.postMetricFailure(successMetricName);
		}
		finally{
			run();
		}
	}
}
