/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.main;

import java.io.IOException;

import com.gammatrace.config.AppConfig;
import com.gammatrace.logging.CustomLogger;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Main {
    private static final Logger logger = CustomLogger.getLogger(Main.class);

    public static void main(String[] args) throws IOException{
    	// Initialize App Config before anything else
    	AppConfig.init();
    	
        /**
         * Initialize spring beans
         */  
        final ApplicationContext context = new FileSystemXmlApplicationContext("file:spring-configuration/spring-config.xml"); 
    }
}
