/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.metricmanager;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filtermanager.AbstractFilterManager;

public class DummyMetricManager implements AbstractMetricManager{
	AbstractFilterManager filterManager;
	
	public DummyMetricManager (AbstractFilterManager filterManager){
		this.filterManager = filterManager;
	}
	
	public Map<Taxonomy, List<Map<String, Object>>> filterAndCalculate (Collection<Map<String, Object>> trades){
		for (Map<String, Object> tradeMap : trades){
			filterManager.shouldReprice(tradeMap); // the debug filtermanager keeps track of the trades we've seen
		}
		return null;
	}
}
