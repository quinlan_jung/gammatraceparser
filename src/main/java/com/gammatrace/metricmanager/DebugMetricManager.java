/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.metricmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.gammatrace.commonutils.SupportedUtils;
import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Metric;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filtermanager.AbstractFilterManager;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

// Pass it a bunch of rows -> filter, then generate all the metrics -> return set of repriced trades
public class DebugMetricManager implements AbstractMetricManager{
	Logger logger = CustomLogger.getLogger(MetricManager.class);
	public static Map<Long, String> errorMessages = new HashMap<Long, String>(); //testing
	public static Set<Long> successfulTrades = new HashSet<Long>(); //testing
	public static List<Map<String, Object>> allTrades = new ArrayList<Map<String, Object>> ();
	ObjectMapper mapper = new ObjectMapper();
	AbstractFilterManager filterManager;

	public DebugMetricManager (AbstractFilterManager filterManager){
		this.filterManager = filterManager;
	}
	
	public Map<Taxonomy, List<Map<String, Object>>> filterAndCalculate(Collection<Map<String, Object>> trades) {
		// Filter the trades
		List<Trade> acceptedTrades = new ArrayList<Trade> ();
		for (Map<String, Object> tradeMap : trades) {
			if (filterManager.shouldReprice(tradeMap)) {
				Trade trade = mapper.convertValue(tradeMap, Trade.class);
				if (!supported(trade)){
					continue;
				}
				acceptedTrades.add(trade);
			}
		}
		
		// Sort to optimize repricing
		Collections.sort(acceptedTrades, new TradeOptimizeComparator());
		
		// Reprice the trades
		int numRepriced = 0;
		Map<Taxonomy, List<Map<String, Object>>> repricedTrades = new HashMap<Taxonomy, List<Map<String, Object>>> ();
		for (Trade acceptedTrade : acceptedTrades){
			numRepriced++;
			logger.info(String.format("Repricing id: %d, attempted reprice: %d out of %d", acceptedTrade.getDissemination_id(), numRepriced, acceptedTrades.size()));
			repriceAndUpdate (acceptedTrade, repricedTrades);
		}
		return repricedTrades;
	}
	
	static Set<Taxonomy> supportedTaxonomies = new HashSet<Taxonomy>();
	static {
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_IRSWAP_OIS);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_CROSSCURRENCY_BASIS);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFIXED);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_IRSWAP_BASIS);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_OPTION_SWAPTION);
		supportedTaxonomies.add(Taxonomy.INTERESTRATE_CAPFLOOR);
	}

	private boolean supported(Trade trade){
		return supportedTaxonomies.contains(Taxonomy.toEnum(trade.getTaxonomy())) && SupportedUtils.isSupportedCurrency(trade);
	}
	
	private void updateTradeMap(Map<String, Object> tradeMap, RepriceResult repriceResult) throws JsonGenerationException, JsonMappingException, IOException{
		tradeMap.put(Metric.COMMON_FIXED_FAIR_RATE.toString(), repriceResult.getFixedFairRate());
		tradeMap.put(Metric.EFFECTIVE_DATE_FIXED_FAIR_RATE.toString(), repriceResult.getEffectiveDateRate());
		tradeMap.put(Metric.END_DATE_FIXED_FAIR_RATE.toString(), repriceResult.getEndDateRate());
		tradeMap.put(Metric.FIXED_DELTA.toString(), repriceResult.getFixedDelta() == null ? null : repriceResult.getFixedDelta().toJsonString());
		tradeMap.put(Metric.SPREAD_DELTA.toString(), repriceResult.getSpreadDelta() == null ? null : repriceResult.getSpreadDelta().toJsonString());
		tradeMap.put(Metric.XCCY_DELTA.toString(), repriceResult.getXccyDelta() == null ? null : repriceResult.getXccyDelta().toJsonString());
		tradeMap.put(Metric.VEGA_MATRIX.toString(), repriceResult.getVegaMatrix() == null ? null : repriceResult.getVegaMatrix().toJsonString());
		tradeMap.put(Metric.VOLATILITY.toString(), repriceResult.getVolatility());
		tradeMap.put(Metric.FLAT_DELTA.toString(), repriceResult.getFlatDelta() == null ? null : repriceResult.getFlatDelta().toJsonString());
	}
	
	private void repriceAndUpdate (Trade trade, Map<Taxonomy, List<Map<String, Object>>> repricedTrades){
		try{
			Taxonomy taxonomy = Taxonomy.toEnum(trade.getTaxonomy());
			
			@SuppressWarnings("unchecked")
			Map<String, Object> tradeMap = mapper.convertValue(trade, Map.class);
			
			allTrades.add(tradeMap);
			
			RepriceResult repriceResult = Repricer.reprice(trade);
			
			updateTradeMap(tradeMap, repriceResult);
			Utils.putInMapList(repricedTrades, taxonomy, tradeMap);
			successfulTrades.add((Long) tradeMap.get(DtccValue.DISSEMINATION_ID.toString()));
		}
		catch (Exception e){
			StringBuilder errorMessage = new StringBuilder(e.getMessage() + " ");
			for (StackTraceElement se : e.getStackTrace()){
				errorMessage.append(se.toString());
			}
			errorMessages.put(trade.getDissemination_id(), errorMessage.toString());
			logger.error(String.format("Exception: id: %d, message: %s", trade.getDissemination_id(), e.getMessage()), e);
		}
	}
}

