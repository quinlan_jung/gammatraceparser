/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.metricmanager;

import java.util.Comparator;

import com.gammatrace.datamodel.Trade;

public class TradeOptimizeComparator implements Comparator<Trade>{
	@Override
	// Sort descending on execution timestamp
	public int compare(Trade o1, Trade o2) {
		return (int) (o2.getExecution_timestamp() - o1.getExecution_timestamp());
	}
}