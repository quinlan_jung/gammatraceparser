/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.metricmanager;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.gammatrace.enums.Taxonomy;

public interface AbstractMetricManager {
	public Map<Taxonomy, List<Map<String, Object>>> filterAndCalculate (Collection<Map<String, Object>> trades);
}
