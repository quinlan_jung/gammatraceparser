/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.metricmanager;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.enums.Metric;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filtermanager.AbstractFilterManager;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

// Pass it a bunch of rows -> filter, then generate all the metrics -> return set of repriced trades
public class MetricManager implements AbstractMetricManager{
	Logger logger = CustomLogger.getLogger(MetricManager.class);
	AbstractFilterManager filterManager;

	public MetricManager (AbstractFilterManager filterManager){
		this.filterManager = filterManager;
	}
	
	public Map<Taxonomy, List<Map<String, Object>>> filterAndCalculate(Collection<Map<String, Object>> trades) {
		Map<Taxonomy, List<Map<String, Object>>> repricedTrades = new HashMap<Taxonomy, List<Map<String, Object>>> ();
		ObjectMapper mapper = new ObjectMapper();
		int numRepriced = 0;
		for (Map<String, Object> tradeMap : trades) {
			if (filterManager.shouldReprice(tradeMap)) {
				Trade trade = mapper.convertValue(tradeMap, Trade.class);
				numRepriced++;
				logger.info(String.format("Repricing id: %d, total repriced: %d", trade.getDissemination_id(), numRepriced));
				repriceAndUpdate (trade, tradeMap, repricedTrades);
			}
		}
		return repricedTrades;
	}
	
	private void updateTradeMap(Map<String, Object> tradeMap, RepriceResult repriceResult) throws JsonGenerationException, JsonMappingException, IOException{
		tradeMap.put(Metric.COMMON_FIXED_FAIR_RATE.toString(), repriceResult.getFixedFairRate());
		tradeMap.put(Metric.EFFECTIVE_DATE_FIXED_FAIR_RATE.toString(), repriceResult.getEffectiveDateRate());
		tradeMap.put(Metric.END_DATE_FIXED_FAIR_RATE.toString(), repriceResult.getEndDateRate());
		tradeMap.put(Metric.FIXED_DELTA.toString(), repriceResult.getFixedDelta() == null ? null : repriceResult.getFixedDelta().toJsonString());
		tradeMap.put(Metric.SPREAD_DELTA.toString(), repriceResult.getSpreadDelta() == null ? null : repriceResult.getSpreadDelta().toJsonString());
		tradeMap.put(Metric.XCCY_DELTA.toString(), repriceResult.getXccyDelta() == null ? null : repriceResult.getXccyDelta().toJsonString());
		tradeMap.put(Metric.VEGA_MATRIX.toString(), repriceResult.getVegaMatrix() == null ? null : repriceResult.getVegaMatrix().toJsonString());
		tradeMap.put(Metric.VOLATILITY.toString(), repriceResult.getVolatility());
	}
	
	private void repriceAndUpdate (Trade trade, Map<String, Object> tradeMap, Map<Taxonomy, List<Map<String, Object>>> repricedTrades){
		try{
			Taxonomy taxonomy = Taxonomy.toEnum(trade.getTaxonomy());
			RepriceResult repriceResult = Repricer.reprice(trade);
			updateTradeMap(tradeMap, repriceResult);
			Utils.putInMapList(repricedTrades, taxonomy, tradeMap);
		}
		catch (Exception e){
			logger.error(String.format("Exception: id: %d, message: %s", trade.getDissemination_id(), e.getMessage(), e));
		}
	}
}
