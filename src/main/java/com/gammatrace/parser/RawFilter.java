/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gammatrace.commonutils.Constants;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.exceptions.UnparsableException;
import com.gammatrace.logging.CustomLogger;

public class RawFilter {
	Logger logger = CustomLogger.getLogger(RawFilter.class);
	final static String EXECUTION_PATTERN = "YYYY-MM-dd'T'HH:mm:ss";
	final static String YYYYMMDD_PATTERN = "YYYY-MM-dd";
	
	public RawFilter (){}
	
	private long parseDateString(String value, String pattern) throws IllegalArgumentException{
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		DateTime dt = formatter.withZone(DateTimeZone.UTC).parseDateTime(value);
		return dt.getMillis() / DateTimeConstants.MILLIS_PER_SECOND;
		
	}
	
	private Double parseDoubleFromString(String value) {
		value = value.replaceAll(",", "");
		return value.equals(Constants.EMPTY_STRING) ? null : Double.parseDouble(value);
	}
	
	private Long parseLongFromString (String value){
		value = value.replaceAll(",", "");
		return value.equals(Constants.EMPTY_STRING) ? null : Long.parseLong(value);
	}
	
	/**
	 * Extract the notional amount from a string of format XXXX+ or XXXX
	 * @return the notional amount
	 * @exception throws IllegalArgumentException if it is neither of the specified input formats
	 */
	private Long parseNotionalFromString(String notional){
		if (notional.equals(Constants.EMPTY_STRING)){
			return null;
		}
		notional = notional.replaceAll(",", "");
		Pattern pattern = Constants.NOTIONAL;
		Matcher matcher = pattern.matcher(notional);
		while (matcher.find()) {
			return Long.parseLong(matcher.group(1));
		}
		throw new IllegalArgumentException("Invalid amount given: " + notional);
	}
	
	/**
	 * Find out if notional is of format XXXX+ or XXXX
	 * @return true if format is XXXX+, false if XXXX
	 * @exception throws IllegalArgumentException if it is neither of these formats
	 */
	private boolean isOverflow (String notional){
		if (notional.equals(Constants.EMPTY_STRING)){
			return false;
		}
		notional = notional.replaceAll(",", "");
		Pattern pattern = Constants.NOTIONAL;
		Matcher matcher = pattern.matcher(notional);
		String overflowSymbol = "+";
		while (matcher.find()) {
			return overflowSymbol.equals(matcher.group(2));
		}
		throw new IllegalArgumentException("Invalid amount given: " + notional);
	}

	/**
	 * Populate the given MapSqlParameterSource
	 * @guarantee Strings will never be null
	 * @guarantee Primitives (Long, Double, etc) will always be wellformed or null
	 * @guarantee Dissemination Id will never be null
	 * 
	 * @throws UnparsableException 
	 */
	public void populateParameterSource(Map<String, Object> tradeMap, String columnName, String value) throws UnparsableException{
		value = value.trim();

		try{			
			Object parsedValue;
			DtccValue dtccValue = DtccValue.toEnum(columnName);

			// The non-default cases are not Strings - they must be a value, or null
			switch(dtccValue){
			case DISSEMINATION_ID:
				parsedValue =  Long.parseLong(value);
				break;
			case ORIGINAL_DISSEMINATION_ID:
				parsedValue = parseLongFromString(value);
				break;
			case EXECUTION_TIMESTAMP:
				parsedValue = parseDateString(value, EXECUTION_PATTERN);
				break;
			case EFFECTIVE_DATE:
				parsedValue = parseDateString(value, YYYYMMDD_PATTERN);
				break;
			case END_DATE:
				parsedValue = parseDateString(value, YYYYMMDD_PATTERN);
				break;
			case PRICE_NOTATION:
				parsedValue =  parseDoubleFromString(value);
				break;
			case ADDITIONAL_PRICE_NOTATION:
				parsedValue = parseDoubleFromString(value);
				break;
			case ROUNDED_NOTIONAL_AMOUNT_1:
				parsedValue = parseNotionalFromString(value);
				tradeMap.put(DtccValue.ROUNDED_NOTIONAL_OVERFLOW_FLAG_1.toString(), isOverflow(value));
				break;
			case ROUNDED_NOTIONAL_AMOUNT_2:
				parsedValue = parseNotionalFromString(value);
				tradeMap.put(DtccValue.ROUNDED_NOTIONAL_OVERFLOW_FLAG_2.toString(), isOverflow(value));
				break;
			case OPTION_STRIKE_PRICE:
				parsedValue =  parseDoubleFromString(value);
				break;
			case OPTION_PREMIUM:
				parsedValue =  parseDoubleFromString(value);
				break;
			case OPTION_EXPIRATION_DATE:
				parsedValue = value.equals(Constants.EMPTY_STRING) ? null : parseDateString(value, YYYYMMDD_PATTERN);		
				break;
			case PRICE_NOTATION2:
				parsedValue =  parseDoubleFromString(value);
				break;
			case PRICE_NOTATION3:
				parsedValue =  parseDoubleFromString(value);
				break;
			default: // All the Strings
				parsedValue = value;
				break;
			}
			tradeMap.put(columnName, parsedValue);
		}
		catch(Exception e){
			exceptionHandler (e, columnName, value, tradeMap);
		}
	}
	
	private void exceptionHandler (Exception e, String columnName, String value, Map<String, Object> tradeMap) throws UnparsableException {
		logger.info("Caught Exception on column " + columnName + " on value "+value);
		
		// insert a NULL value if we do not require the column to be precisely formatted
		if (!columnName.equalsIgnoreCase("dissemination_id")){
			logger.info("Inserting a null value");
			tradeMap.put(columnName, null);
		}
		else{
			logger.error("Invalid disemmination id. Aborting add.");
			throw new UnparsableException("Invalid disemmination id. Aborting add.");
		}
	}
}
