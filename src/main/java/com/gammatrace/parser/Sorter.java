/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVParser;

import com.gammatrace.enums.DtccValue;
import com.gammatrace.exceptions.UnparsableException;
import com.gammatrace.logging.CustomLogger;

/**
 * Sorts a trade by putting in the correct map for insertion or deletion
 * If it needs to be repriced, it will be put in the repriced map
 * A new Sorter should be made for every XML document we parse
 *
 */
public class Sorter {
	Logger logger = CustomLogger.getLogger(Sorter.class);
	final static String [] columns = DtccValue.getPulledColumnsAsString();
	final static int DISSEMINATION_ID_POS = DtccValue.getPulledPosition(DtccValue.DISSEMINATION_ID);
	final static int ORIGINAL_DISSEMINATION_ID_POS = DtccValue.getPulledPosition(DtccValue.ORIGINAL_DISSEMINATION_ID);
	final static int ACTION_POS = DtccValue.getPulledPosition(DtccValue.ACTION);
	final static int TAXONOMY_POS = DtccValue.getPulledPosition(DtccValue.TAXONOMY);

	// Parse each item in our feed, storing the ones to insert in insertRows
	List <Map<String, Object>> insertRows = new ArrayList <Map<String, Object>> ();

	// Store the rows we want to delete in deleteRows
	// Key: original dissemination id, Value: Taxonomy (can be unsupported)
	Map <String, Set<Long>> deleteRows = new HashMap <String, Set<Long>> ();
	
	public Sorter(){}
	
	public List<Map<String, Object>> getInsertRows() {
		return insertRows;
	}

	public Set<Long> getDeleteRowIds() {
		Set<Long> unionSet = new HashSet<Long> ();
		for (Set<Long> set : deleteRows.values()){
			unionSet.addAll(set);
		}
		return unionSet;
	}
	
	public Map<String, Set<Long>> getDeleteRows(){
		return deleteRows;
	}
	
	public void clearResults(){
		insertRows.clear();
		deleteRows.clear();
	}
	
	/**
	 * Mark a row for deletion
	 */
	private void addDeleteRecord(String [] values) throws NumberFormatException{
		long disseminationId = Long.parseLong(values[ORIGINAL_DISSEMINATION_ID_POS].trim());
		String taxonomy = values[TAXONOMY_POS].trim();		

		// Add id to delete map
		Set <Long> deleteIds;
		if (!deleteRows.containsKey(taxonomy)){
			deleteIds = new HashSet<Long> ();
			deleteRows.put(taxonomy, deleteIds);
		}
		else{
			deleteIds = deleteRows.get(taxonomy);
		}
		deleteIds.add(disseminationId);
	}

	/**
	 * Create an insert record
	 * @return MapParameterSource on success, null if ERROR or SKIP
	 * @throws UnparsableException 
	 */
	private Map<String, Object> createInsertRecord(String [] values) throws UnparsableException{
		RawFilter rawFilter = new RawFilter();
		Map<String, Object> tradeMap = new HashMap<String, Object>();

		for (int i = 0; i < Math.min(columns.length, values.length); i++) {
			rawFilter.populateParameterSource(tradeMap, columns[i], values[i]);
		}
		
		return tradeMap;
	}

	/**
	 * Mark a row for insertion
	 * @throws UnparsableException 
	 */
	private void addInsertRecord(String [] values) throws UnparsableException {
		Map<String, Object> tradeMap = createInsertRecord(values);
		// Add the row to eventually get inserted in Trade table
		insertRows.add(tradeMap);
	}

	/**
	 * Figure out whether we should insert and/or delete for NEW, CORRECT and CANCEL entries
	 */
	public void processRecord(String csv) throws IOException {
		try {
			// Do some initial processing
			csv = csv.replace("\n", "").replace("\r", "").trim();
			CSVParser csvParser = new CSVParser();
			String[] values = csvParser.parseLine(csv);
			
			// Figure out if entry is CORRECT, NEW or CANCEL
			String action = values[ACTION_POS];
			if (action.equalsIgnoreCase("NEW") || action.equalsIgnoreCase("CORRECT")) {
				addInsertRecord(values); // Mark trade for insertion
			}
			else if (action.equalsIgnoreCase("CANCEL")) {
				addDeleteRecord(values); // Mark trade for deletion
			}
		}
		catch (NumberFormatException | UnparsableException e) {
			logger.error(String.format("Unable to process record %s", csv), e);
			return;
		}	
	}
}
