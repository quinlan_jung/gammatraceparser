/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

import com.gammatrace.helpers.SqlUtils;
import com.gammatrace.logging.CustomLogger;

public class DBOrchestrator {
	Logger logger = CustomLogger.getLogger(DBOrchestrator.class);
	SqlUtils sqlUtils;

	public DBOrchestrator (SqlUtils sqlUtils) {
		this.sqlUtils = sqlUtils;
	}
	
	public void modifyFromDisk(String table, String[] columns, Collection<Map<String, Object>> insertRows, String deleteKey, Collection<Long> deleteIds) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		logger.info(String.format("Inserting %d rows", insertRows.size()));
		if (insertRows.size() > 0){
			insertFromDisk(table, columns, insertRows);
		}

		logger.info(String.format("Deleting %d rows", deleteIds.size()));
		if (deleteIds.size() > 0){
			deleteInMemory(table, deleteKey, deleteIds);
		}
	}

	public void insertFromDisk(String table, String[] columns, Iterable<Map<String, Object>> tradeMaps) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		StringWriter sw = new StringWriter();
		CSVWriter writer = new CSVWriter(sw);

		List<String[]> csv = new ArrayList<String[]>();

		// Add header
		csv.add(columns);
		
		// Create record
		for (Map<String, Object> trade : tradeMaps){
			String [] fieldValues = new String[columns.length];
			for (int i = 0; i < columns.length; i++){
				Object fieldValue = trade.get(columns[i]);
				// All null and blank values get stored as NULL
				fieldValues[i] = (fieldValue == null || fieldValue.toString().isEmpty()) ? null : fieldValue.toString();
			}
			csv.add(fieldValues);
		}

		// Write the record to file
		writer.writeAll(csv);
		csv.clear();
		sqlUtils.insertRows(table, new StringReader(sw.toString()));
		
		// close the writer
		writer.close();
	}

	public void deleteInMemory(String table, String deleteKey, Iterable<Long> deleteIds) {
		sqlUtils.deleteRows(deleteIds, deleteKey, table);
	}
}
