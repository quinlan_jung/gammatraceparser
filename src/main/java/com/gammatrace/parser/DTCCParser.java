/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gammatrace.commonutils.TimeUtils;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.logging.CustomLogger;

/**
 * Process a DTCC xml webpage
 */
public class DTCCParser {
	Logger logger = CustomLogger.getLogger(DTCCParser.class);
	final static int DISSEMINATION_ID_POS = DtccValue.getPulledPosition(DtccValue.DISSEMINATION_ID);

	final static String PUBDATE = "pubDate";
	final static String ITEM = "item";
	final static String TITLE = "title";
	final static String DESCRIPTION = "description";
	final static String PUBDATE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss Z";
	final URL url;
	
	DateTimeFormatter formatter = DateTimeFormat.forPattern(PUBDATE_PATTERN).withOffsetParsed();
	boolean decidedToParseItem = false;
	DateTime feedPubDate;
	DateTime watermarkPubDate;
	Sorter sorter;

	public DTCCParser(String url, DateTime watermarkPubDate, Sorter sorter) throws MalformedURLException {
		this.url = new URL(url);
		this.watermarkPubDate = watermarkPubDate;
		this.sorter = sorter;
	}
	
	private boolean isNewPublication(DateTime dt) {
		return watermarkPubDate.isBefore(dt);
	}
	
	private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
			throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}

	private InputStream read() throws IOException {
		return url.openStream();
	}

	/**
	 * Parse the XML elements from the specified DTCC website
	 * @modifies Sorter
	 * @return feedPubDate
	 */
	public DateTime parsePage() throws XMLStreamException, IOException {		
		// First create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		
		// Setup a new eventReader with the target website
		InputStream in = read();
		XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

		// Check if the pubDate posted is new
		// -> Break out of loop and continue with parsing if it is new
		// -> Exit function if it is old
		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();
			if (event.isStartElement()) {
				String localPart = event.asStartElement().getName().getLocalPart();

				if (localPart.equals(PUBDATE)) {
					String pubDate = getCharacterData(event, eventReader).trim();
					feedPubDate = formatter.parseDateTime(pubDate);
					feedPubDate = feedPubDate.withZone(DateTimeZone.UTC);

					// The pubDate is new, so we exit our current loop and enter
					// the next one
					if (isNewPublication(feedPubDate)) {
						logger.info(String.format("Found new publication, marked at date %s", feedPubDate.toString()));
						break;
					}

					// The pubDate is old, so we give up parsing altogether
					else {
						logger.info(String.format("Found old publication, marked at date %s. Skipping.", feedPubDate.toString()));
						return feedPubDate;
					}
				}
			}
		}
		
		// Now that we know this info is new, go over each XML element and parse
		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();
			if (event.isStartElement()) {
				String localPart = event.asStartElement().getName().getLocalPart();

				if (localPart.equals(ITEM)) {
					decidedToParseItem = true;
					continue;
				}

				// Paranoia: Just in case the pubDate in an item isnt the same
				// as the pubDate posted at the beginning
				else if (decidedToParseItem && localPart.equals(PUBDATE)) {
					String pubDate = getCharacterData(event, eventReader).trim();
					DateTime dt = formatter.parseDateTime(pubDate);
					dt = dt.withZone(DateTimeZone.UTC);

					// The pubDate is new, as we expected, so we continue what
					// we were doing
					if (isNewPublication(dt)) {
						continue;
					}

					// The pubDate is old, so we give up parsing the item
					else {
						decidedToParseItem = false;
						continue;
					}
				}

				else if (decidedToParseItem && localPart.equals(DESCRIPTION)) {
					String description = getCharacterData(event, eventReader);
					// We've hit the main body of a record. We parse it.
					sorter.processRecord(description);
					continue;
				}
			} 
			
			else if (event.isEndElement()) {
				String localPart = event.asEndElement().getName().getLocalPart();

				if (localPart.equals(ITEM)) {
					// We've hit the end of an item, resetting our flags
					decidedToParseItem = false;
					continue;
				}
			}
		}
		return feedPubDate;
	}
}
