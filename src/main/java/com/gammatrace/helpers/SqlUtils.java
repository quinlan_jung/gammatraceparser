/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.helpers;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.gammatrace.commonutils.StringUtils;
import com.gammatrace.config.AppConfig;
import com.gammatrace.database.DatabaseClient;
import com.gammatrace.logging.CustomLogger;

public class SqlUtils {
	Logger logger = CustomLogger.getLogger(SqlUtils.class);
	DatabaseClient databaseClient;
	
	public SqlUtils(DatabaseClient databaseClient){
		this.databaseClient = databaseClient;
	}
	
	public void addNewColumn(String tableName, String columnName, Class<?> clazz){
		String columnType = getColumnType(clazz);
		String sql = "ALTER TABLE ${tableName} ADD ${columnName} ${columnType}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("columnName", columnName);
		strSubstitutions.put("columnType", columnType);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());

	}
	
	private String getColumnType (Class<?> clazz){
		if (clazz == Boolean.class){
			return "boolean";
		}
		else if (clazz == Double.class){
			return "double precision";
		}
		else if (clazz == Float.class){
			return "real";
		}
		else if (clazz == String.class){
			return "varchar";
		}
		else if (clazz == Integer.class){
			return "int";
		}
		else if (clazz == Long.class){
			return "bigint";
		}
		else{
			throw new IllegalArgumentException(String.format("Class %s cannot be mapped to a sql column type", clazz.toString()));
		}
	}
	
	public void dropTable(String tableName){
		String sql = "DROP TABLE IF EXISTS ${tableName}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	public void createTable(String tableName, String likeTable){
		String sql = "CREATE TABLE IF NOT EXISTS ${tableName} (LIKE ${likeTable} INCLUDING ALL)";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("likeTable", likeTable);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	public int totalRows(String tableName, Iterable<String> taxonomy){
		String sql = "SELECT COUNT(1) FROM ${tableName} WHERE ${whereClause}";
		List<String> taxonomyClauses = new ArrayList<String>();
		for (String t : taxonomy){
			taxonomyClauses.add(String.format("taxonomy = '%s'", t));
		}
		String whereClause = StringUtils.generateDelimitedString(taxonomyClauses, " OR ", null);
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("whereClause", whereClause);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		return databaseClient.getNamedParameterJdbcTemplate().queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}
	
	public int totalRows(String tableName, String taxonomy){
		String sql = "SELECT COUNT(1) FROM ${tableName} WHERE taxonomy = '${taxonomy}'";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("taxonomy", taxonomy);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		return databaseClient.getNamedParameterJdbcTemplate().queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}
	
	public int totalRows(String tableName){
		String sql = "SELECT COUNT(1) FROM ${tableName}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		return databaseClient.getNamedParameterJdbcTemplate().queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}
	
	public int testConnection(){
		return databaseClient.getNamedParameterJdbcTemplate().queryForObject("SELECT 1", new MapSqlParameterSource(), Integer.class);
	}
	
	// TODO: run a poller on this every 6 hours or so
	public void maintainTable(String tableName){
		clusterTable(tableName);
		analyzeTable(tableName);
	}
	
	public void analyzeTable(String tableName){
		logger.info("ANALYZING table " + tableName);
		String sql = "ANALYZE ${tableName}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	public void clusterTable (String tableName){
		logger.info("CLUSTERING table " + tableName);
		String indexName = AppConfig.getProperty(String.format("%s_index", tableName));
		if (indexName == null){
			throw new IllegalArgumentException("Index does not exist on "+tableName);
		}
		String sql = "CLUSTER ${tableName} using ${indexName}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("indexName", indexName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	public void truncateTable(String tableName){
		String sql = "TRUNCATE TABLE ${tableName}";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	// Delete rows from the main TRADE_TABLE
	public void deleteRows(Iterable <Long> disseminationIds, String columnName, String tableName) {
		String sql = "DELETE FROM ${tableName} WHERE ${column} IN (${values})";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("column", columnName);
		strSubstitutions.put("values", StringUtils.generateDelimitedString(disseminationIds, ",", null));
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
	
	// Insert rows into the main TRADE_TABLE
	public void insertRows(Collection <MapSqlParameterSource> rows, String [] columns, String tableName) {
		String sql = "INSERT into ${tableName} (${columns}) VALUES (${values})";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		strSubstitutions.put("columns", StringUtils.generateDelimitedString(columns, ",", null));
		strSubstitutions.put("values", StringUtils.generateDelimitedString(columns, ",", ":"));
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		
		databaseClient.getNamedParameterJdbcTemplate().batchUpdate(sql, rows.toArray(new MapSqlParameterSource[rows.size()]));			
	}
	
	public void insertRows(String tableName, Reader from) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException, IOException{
		String sql = "COPY ${tableName} FROM STDIN WITH DELIMITER ',' CSV HEADER";
		Map<String, String> strSubstitutions = new HashMap<String, String>();
		strSubstitutions.put("tableName", tableName);
		sql = StrSubstitutor.replace(sql, strSubstitutions);
		databaseClient.copyFrom(sql, from);			
	}
}
