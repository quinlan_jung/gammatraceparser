/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.helpers;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.gammatrace.database.DatabaseClient;
import com.gammatrace.logging.CustomLogger;
import com.google.common.base.Preconditions;

public class WatermarkHelper {
	Logger logger = CustomLogger.getLogger(WatermarkHelper.class);
	DatabaseClient databaseClient;
	String watermark;
	
	public WatermarkHelper(DatabaseClient databaseClient, String watermark){
		this.databaseClient = databaseClient;
		this.watermark = watermark;
	}
	
	public DateTime getWatermarkFromDatabase() {
		String sql = String.format("SELECT value FROM config WHERE key = '%s'", watermark);
		long timestamp = (long) databaseClient.getNamedParameterJdbcTemplate().queryForObject(sql, new MapSqlParameterSource(), Long.class);
		return new DateTime(timestamp * DateTimeConstants.MILLIS_PER_SECOND, DateTimeZone.UTC);
	}

	public void updateWatermark(DateTime feedPubDate) {
		Preconditions.checkNotNull(feedPubDate);
		long timestamp = feedPubDate.getMillis() / DateTimeConstants.MILLIS_PER_SECOND;
		String sql = String.format("UPDATE config SET value = '%s' WHERE key = '%s'", timestamp, watermark);
		databaseClient.getNamedParameterJdbcTemplate().update(sql, new MapSqlParameterSource());
	}
}
