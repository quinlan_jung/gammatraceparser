/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.helpers;

import com.gammatrace.commonutils.StringUtils;
import com.gammatrace.commonutils.Utils;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Metric;
import com.gammatrace.enums.Taxonomy;

public class TableLogic {
	
	public static String getArchivalTable (){
		return "interest_swap";
	}
	
	public static String [] getArchivalColumnsAsString(){
		return StringUtils.getColumnsAsString(DtccValue.DTCC_DB_COLUMN_ARRAY);
	}
	
	// Testing only
	public static String [] getAnalysisColumnsAsString(){
		return Utils.concatenate(StringUtils.getColumnsAsString(DtccValue.DTCC_DB_COLUMN_ARRAY), StringUtils.getColumnsAsString(Metric.METRIC_ARRAY));
	}
	
	public static String getRepriceTableName(Taxonomy taxonomy){
		String taxonomyDbFormat = taxonomy.toString().toLowerCase().replace(":", "_");
		return String.format("metric_%s", taxonomyDbFormat);
	}
	
	public static String[] getRepriceColumns(Taxonomy taxonomy){
		DtccValue[] dtccColumns = new DtccValue[0];
		Metric[] metricColumns = new Metric[0];
		switch(taxonomy){
		case INTERESTRATE_IRSWAP_FIXEDFLOAT:
			dtccColumns = DtccValue.DTCC_INTERESTRATE_IRSWAP_FIXEDFLOAT_COLUMN_ARRAY;
			metricColumns = Metric.METRIC_INTERESTRATE_IRSWAP_FIXEDFLOAT_COLUMN_ARRAY;
			break;
		case INTERESTRATE_IRSWAP_OIS:
		case INTERESTRATE_IRSWAP_BASIS:
			dtccColumns = DtccValue.DTCC_INTERESTRATE_IRSWAP_OIS_COLUMN_ARRAY;
			metricColumns = Metric.METRIC_INTERESTRATE_IRSWAP_OIS_COLUMN_ARRAY;
			break;
		case INTERESTRATE_CROSSCURRENCY_BASIS:
		case INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT:
		case INTERESTRATE_CROSSCURRENCY_FIXEDFIXED:
			dtccColumns = DtccValue.DTCC_INTERESTRATE_XCCY_COLUMN_ARRAY;
			metricColumns = Metric.METRIC_INTERESTRATE_XCCY_COLUMN_ARRAY;
			break;
		case INTERESTRATE_OPTION_SWAPTION:
		case INTERESTRATE_CAPFLOOR:
			dtccColumns = DtccValue.DTCC_INTERESTRATE_OPTION_COLUMN_ARRAY;
			metricColumns = Metric.METRIC_INTERESTRATE_OPTION_COLUMN_ARRAY;
			break;
		default:
			dtccColumns = DtccValue.DTCC_DB_COLUMN_ARRAY;
			metricColumns = Metric.METRIC_ARRAY;
			break;
		}		
		return Utils.concatenate(StringUtils.getColumnsAsString(dtccColumns), StringUtils.getColumnsAsString(metricColumns));
	}
}
