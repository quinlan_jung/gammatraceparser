/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.exceptions;

/**
 * An exception which is thrown when processing a row, and cannot be recovered from
 * The row must be discarded as a result
 *
 */
public class UnparsableException extends Exception{

	public UnparsableException(){
		super();
		System.out.println("Received an unparseable error.");
	}
	
	public UnparsableException(String err){
		super(err);
		System.out.println("Received an unparseable error because: "+err);
	}
}
