/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.stats;

import java.util.Arrays;
import java.util.Date;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;

public class CloudwatchClient {
	AmazonCloudWatchClient client;
	String namespace;
	
	public CloudwatchClient(AWSCredentials awsCredentials, Region region, String namespace){
		this.client = new AmazonCloudWatchClient(awsCredentials);
		this.client.setRegion(region);
		this.namespace = namespace;
	}
	
	public void postMetric(String metric, Double value){
		PutMetricDataRequest request = new PutMetricDataRequest();
		request.setNamespace(namespace);
		MetricDatum metricDatum = new MetricDatum();
		metricDatum.setMetricName(metric);
		metricDatum.setTimestamp(new Date());
		metricDatum.setValue(value);
		request.setMetricData(Arrays.asList(metricDatum));
		client.putMetricData(request);
	}
	
	public void postMetricSuccess(String metric){
		postMetric(metric, 1d);
	}
	
	public void postMetricFailure(String metric){
		postMetric(metric, 0d);
	}
}
