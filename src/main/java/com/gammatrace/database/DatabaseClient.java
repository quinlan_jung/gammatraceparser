/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.database;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

import org.postgresql.PGConnection;
import org.postgresql.copy.CopyManager;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.gammatrace.config.AppConfig;
import com.mchange.v2.c3p0.C3P0ProxyConnection;
import com.mchange.v2.c3p0.ComboPooledDataSource;

// This class will set up a connection to Mysql and offer a means to update a watermark

public class DatabaseClient {
	private static final int MIN_POOL_SIZE = 1;
	private static final int MAX_POOL_SIZE = 10;
	private static final String TEST_QUERY = "SELECT 1";
	private static final int TEST_FREQUENCY = 30;
	private static final int MAX_IDLE_TIME = 60 * 60 * 2; // 2 hours

	ComboPooledDataSource comboPooledDataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	String host = AppConfig.getProperty("db-host");
	int port = 5432;
	String dbName =AppConfig.getProperty("db-name");
	String driver = "org.postgresql.Driver";
	boolean verifyCa = false;
	String jdbcUrl;
	String userMode;
	
	static{
		//TODO: TRACK SSL CERT ISSUE HERE: https://forums.aws.amazon.com/thread.jspa?messageID=604333
		//System.setProperty("javax.net.ssl.trustStore",AppConfig.getProperty("keystore-name"));
		//System.setProperty("javax.net.ssl.trustStorePassword",AppConfig.getProperty("keystore-password"));
	}
	
	public DatabaseClient(String mode) throws SQLException, ClassNotFoundException, PropertyVetoException {
		this.userMode = mode;
		init();
	}

	public DatabaseClient(String mode, boolean verifyCa) throws SQLException, ClassNotFoundException, PropertyVetoException {
		this.userMode = mode;
		this.verifyCa = verifyCa;
		init();
	}
	
	private void init() throws PropertyVetoException{
		jdbcUrl = verifyCa ? String.format("jdbc:postgresql://%s:%d/%s?ssl=true", host, port, dbName) : String.format("jdbc:postgresql://%s:%d/%s?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory", host, port, dbName);
		
		comboPooledDataSource = new ComboPooledDataSource();
		comboPooledDataSource.setDriverClass(driver);
		comboPooledDataSource.setJdbcUrl(jdbcUrl);
		
		if (userMode.equalsIgnoreCase("rw")){
			comboPooledDataSource.setUser(AppConfig.getProperty("db-rw-user"));
			comboPooledDataSource.setPassword(AppConfig.getProperty("db-rw-password"));
		}
		else {
			comboPooledDataSource.setUser(AppConfig.getProperty("db-ro-user"));
			comboPooledDataSource.setPassword(AppConfig.getProperty("db-ro-password"));
		}
				
		comboPooledDataSource.setMinPoolSize(MIN_POOL_SIZE);
		comboPooledDataSource.setMaxPoolSize(MAX_POOL_SIZE);
		comboPooledDataSource.setPreferredTestQuery(TEST_QUERY);
		comboPooledDataSource.setMaxIdleTime(MAX_IDLE_TIME); // for fresh connections
		comboPooledDataSource.setIdleConnectionTestPeriod(TEST_FREQUENCY);
		
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(comboPooledDataSource);
	}
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
		return this.namedParameterJdbcTemplate;
	}
	
	public void copyFrom(String sql, Reader from) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException, IOException {
		// Cast the connection as a proxy connection
		C3P0ProxyConnection proxycon = (C3P0ProxyConnection) comboPooledDataSource.getConnection();
		// Pass the getCopyAPI (from PGConnection) to a method
		Method m = PGConnection.class.getMethod("getCopyAPI", new Class[] {});
		Object[] arg = new Object[] {};

		// Call rawConnectionOperation, passing the method, the raw_connection,
		// and method parameters, and then cast as CopyManager
		CopyManager cm = (CopyManager) proxycon.rawConnectionOperation(m, C3P0ProxyConnection.RAW_CONNECTION, arg);
		cm.copyIn(sql, from, 512 * 1024);
		proxycon.close();
	}
}
