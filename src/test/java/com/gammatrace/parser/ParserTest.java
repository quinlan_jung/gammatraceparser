/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;

public class ParserTest {
	DTCCParser parser;
	
	DateTime tenYearsAgo = new DateTime(2000, 1,1,1,1,1);
	DateTime now = new DateTime();
	
	@Test
	@Ignore
	public void testFixedFloatBeginning() throws XMLStreamException, IOException{
		//databaseClient.updateWatermark(tenYearsAgo);
		//parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/fixedFloatBeginning.rss", databaseClient);
		//parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.times(1)).addRecord(Mockito.anyString());
	}
	
	/*@Test
	public void testFixedFloatEnd() throws XMLStreamException, IOException{
		parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/fixedFloatEnd.rss", mockDatabaseClient);
		parser.setWatermark(tenYearsAgo);
		parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.times(1)).addRecord(Mockito.anyString());
	}
	
	@Test
	public void testManyFixedFloat() throws XMLStreamException, IOException{
		parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/manyFixedFloat.rss", mockDatabaseClient);
		parser.setWatermark(tenYearsAgo);
		parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.times(4)).addRecord(Mockito.anyString());
	}
	
	@Test
	public void testNoFixedFloat() throws XMLStreamException, IOException{
		parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/noFixedFloat.rss", mockDatabaseClient);
		parser.setWatermark(tenYearsAgo);
		parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.never()).addRecord(Mockito.anyString());
	}
	
	@Test
	public void testExpiredPubDate() throws XMLStreamException, IOException{
		parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/expiredPubDate.rss", mockDatabaseClient);
		parser.setWatermark(now);
		parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.never()).addRecord(Mockito.anyString());
	}
	
	@Test
	public void testViolatedPubDateAssumption() throws XMLStreamException, IOException{
		parser = new DTCCParser("https://s3-us-west-2.amazonaws.com/interest-swap-prototype/dtcc-parser/junit-test-files/violatedPubDate.rss", mockDatabaseClient);
		parser.setWatermark(tenYearsAgo);
		parser.parsePage();
		//Mockito.verify(mockDatabaseClient, Mockito.times(2)).addRecord(Mockito.anyString());
	}*/

}
