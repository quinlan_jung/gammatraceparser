/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.parser;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.gammatrace.config.AppConfig;
import com.gammatrace.database.DatabaseClient;
import com.gammatrace.enums.DtccValue;
import com.gammatrace.enums.Metric;
import com.gammatrace.enums.Taxonomy;
import com.gammatrace.filtermanager.AbstractFilterManager;
import com.gammatrace.filtermanager.DebugFilterManager;
import com.gammatrace.filtermanager.ProductionFilterManager;
import com.gammatrace.helpers.SqlUtils;
import com.gammatrace.helpers.TableLogic;
import com.gammatrace.logging.CustomLogger;
import com.gammatrace.metricmanager.AbstractMetricManager;
import com.gammatrace.metricmanager.DebugMetricManager;
import com.gammatrace.metricmanager.DummyMetricManager;

public class RepricingTest {
	Logger logger = CustomLogger.getLogger(RepricingTest.class);
	DatabaseClient databaseClient; 
	
	@BeforeClass
	public static void setUpClass() throws IOException{
		System.out.println("Deleting logs");
		clearDirectory(CustomLogger.getLoggingPath()); // Make sure that we dont initialize the logger here
		AppConfig.init();
	}
	
	// DO NOT USE UTIL'S METHOD!
	public static void clearDirectory(Path path){
		if (!Files.isDirectory(path)){
			return;
		}
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
			for (Path p : ds) {
				Files.deleteIfExists(p);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUp() throws ClassNotFoundException, SQLException, PropertyVetoException, IOException{
		databaseClient = new DatabaseClient("rw");
	}
	
	@Test
	public void testConnection(){
		SqlUtils sqlUtils = new SqlUtils(databaseClient);
		logger.info("HAS CONNECTION: "+sqlUtils.testConnection());
	}

	@Test
	@Ignore
	public void repriceOneDayData() throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		Sorter sorter = getSortedDayData();
		SqlUtils sqlUtils = new SqlUtils(databaseClient);
		AbstractFilterManager filterManager = new ProductionFilterManager();
		AbstractMetricManager metricManager = new DebugMetricManager(filterManager);
		DBOrchestrator dBOrchestrator = new DBOrchestrator(sqlUtils);

		Map<Taxonomy, List<Map<String, Object>>> repricedInsertRows = metricManager.filterAndCalculate(sorter.getInsertRows());
		for (Entry<Taxonomy, List<Map<String, Object>>> entry : repricedInsertRows.entrySet()){
			Taxonomy taxonomy = entry.getKey();
			sqlUtils.truncateTable(TableLogic.getRepriceTableName(taxonomy));
			
			int totalInserts = entry.getValue().size();
			int totalDeletes = sorter.getDeleteRows().get(taxonomy.toString()).size();
			logger.info(String.format("Taxonomy: %s Total rows successfully parsed: %d", taxonomy.toString(), totalInserts + totalDeletes));
			dBOrchestrator.modifyFromDisk(TableLogic.getRepriceTableName(taxonomy), TableLogic.getRepriceColumns(taxonomy), entry.getValue(), DtccValue.DISSEMINATION_ID.toString(), sorter.getDeleteRows().get(taxonomy.toString()));
			sqlUtils.maintainTable(TableLogic.getRepriceTableName(taxonomy));
			logger.info(String.format("Taxonomy: %s Total rows in interest_swap: %d, Total rows in repricing table: %d", taxonomy.toString(), sqlUtils.totalRows(TableLogic.getArchivalTable(), taxonomy.toString()), sqlUtils.totalRows(TableLogic.getRepriceTableName(taxonomy))));
		
		}
	}
	
	@Test
	public void analyzeRepricedOneDayData() throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		Sorter sorter = getSortedDayData();
		DebugFilterManager filterManager = new DebugFilterManager();
		AbstractMetricManager metricManager = new DebugMetricManager(filterManager);

		metricManager.filterAndCalculate(sorter.getInsertRows());
		
		// Populate analysis table
		createFilterTable();
		Set<Map<String, Object>> insertRows = getFilterResults(DebugMetricManager.allTrades, filterManager.getRejectedtrades());
		
		// Mark all trades as successfully or failed to reprice
		Map<Long, String> errorMessages = DebugMetricManager.errorMessages;
		Set<Long> successfulTrades = DebugMetricManager.successfulTrades;
		for (Map<String, Object> trade : insertRows){
			Long disseminationId = (Long) trade.get(DtccValue.DISSEMINATION_ID.toString());
			if (errorMessages.containsKey(disseminationId)){
				trade.put("repriced", false);
				trade.put("errorMessage", errorMessages.get(disseminationId));
			}
			else if (successfulTrades.contains(disseminationId)){
				trade.put("repriced", true);
			}
		}
		
		insertIntoAnalysisTable(insertRows);
	}
	
	@Test
	public void importOneDayData() throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		Sorter sorter = getSortedDayData();
		SqlUtils sqlUtils = new SqlUtils(databaseClient);
		sqlUtils.truncateTable(TableLogic.getArchivalTable());
		DBOrchestrator dbOrchestrator = new DBOrchestrator(sqlUtils);
		dbOrchestrator.modifyFromDisk(TableLogic.getArchivalTable(), TableLogic.getArchivalColumnsAsString(), sorter.getInsertRows(), DtccValue.DISSEMINATION_ID.toString(), sorter.getDeleteRowIds());
		
		int totalInserts = sorter.getInsertRows().size();
		int totalDeletes = sorter.getDeleteRowIds().size();
		logger.info(String.format("Total rows successfully parsed: %d", totalInserts + totalDeletes));
		sqlUtils.maintainTable(TableLogic.getArchivalTable());
		
		int totalRows = sqlUtils.totalRows(TableLogic.getArchivalTable());
		logger.info(String.format("Table %s now has %d rows, plausibility: %b", TableLogic.getArchivalTable(), totalRows, totalInserts - totalDeletes <= totalRows));
	}
	
	@Test
	@Ignore
	public void filterRepricedTrades() throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		DebugFilterManager filterManager = new DebugFilterManager();
		AbstractMetricManager metricManager = new DummyMetricManager(filterManager);
		Sorter sorter = getSortedDayData();
		
		List<Map<String, Object>> insertRecords = sorter.getInsertRows();
		metricManager.filterAndCalculate(insertRecords);
		
		filterManager.printSummary();
		filterManager.printToFile(Paths.get("./tmp/output.xls"));
		
		// Populate analysis table with filter data
		createFilterTable();
		Set<Map<String, Object>> insertRows = getFilterResults(filterManager.getAcceptedtrades(), filterManager.getRejectedtrades());
		insertIntoAnalysisTable(insertRows);
	}
	
	private void createFilterTable(){
		SqlUtils sqlUtils = new SqlUtils(databaseClient);
		
		String tableName = TableLogic.getArchivalTable() + "_filter_analysis";
		sqlUtils.dropTable(tableName);
		sqlUtils.createTable(tableName, TableLogic.getArchivalTable());
		for (Metric metric : Metric.METRIC_ARRAY){
			sqlUtils.addNewColumn(tableName, metric.toString(), metric.getType());
		}
		sqlUtils.addNewColumn(tableName, "accepted", Boolean.class);
		sqlUtils.addNewColumn(tableName, "rejectedColumn", String.class);
		sqlUtils.addNewColumn(tableName, "repriced", Boolean.class);
		sqlUtils.addNewColumn(tableName, "errorMessage", String.class);
	}
	
	private Set<Map<String, Object>> getFilterResults(Collection<Map<String, Object>> acceptedTrades, Map<DtccValue, Set<Map<String, Object>>> rejectedTrades){
		Set<Map<String, Object>> insertRows = new HashSet<Map<String, Object>> ();
		for (Map<String, Object> acceptedTrade : acceptedTrades){
			acceptedTrade.put("accepted", true);
		}
		insertRows.addAll(acceptedTrades);

		for (Entry<DtccValue, Set<Map<String, Object>>> entry : rejectedTrades.entrySet()){
			for (Map<String, Object> rejectedTrade : entry.getValue()){
				rejectedTrade.put("accepted", false);
				rejectedTrade.put("rejectedColumn", entry.getKey().toString());
				rejectedTrade.put("repriced", null);
			}
			insertRows.addAll(entry.getValue());
		}
		return insertRows;
	}
	
	private void insertIntoAnalysisTable(Set<Map<String, Object>> insertRows) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		String tableName = TableLogic.getArchivalTable() + "_filter_analysis";
		SqlUtils sqlUtils = new SqlUtils(databaseClient);
		DBOrchestrator dBOrchestrator = new DBOrchestrator(sqlUtils);
		
		// Add in the accepted and rejectedColumns into the String array
		String[] analysisColumns = new String[TableLogic.getAnalysisColumnsAsString().length + 4];
		System.arraycopy(TableLogic.getArchivalColumnsAsString(), 0, analysisColumns, 0, TableLogic.getArchivalColumnsAsString().length);
		analysisColumns[analysisColumns.length - 4] = "accepted";
		analysisColumns[analysisColumns.length - 3] = "rejectedColumn";
		analysisColumns[analysisColumns.length - 2] = "repriced";
		analysisColumns[analysisColumns.length - 1] = "errorMessage";
		dBOrchestrator.insertFromDisk(tableName, analysisColumns, insertRows);

		int totalRowsInterestSwap = sqlUtils.totalRows(TableLogic.getArchivalTable(), Taxonomy.stringValues());
		logger.info(String.format("Total Supported Taxonomies: %d, Total Parsed Trades: %d, Plausibility: %b", totalRowsInterestSwap, insertRows.size(), insertRows.size() >= totalRowsInterestSwap));

	}
	
	private Sorter getSortedDayData() throws IOException{
		Sorter sorter = new Sorter();
		
		BufferedReader br = new BufferedReader(new FileReader("./resources/parserTestFiles/test_data.csv"));
		String line;
		int lineNumber = 0;
		logger.info("Starting the import");
		while ((line = br.readLine()) != null) {
		   if (lineNumber == 0){ // skip the header
			   lineNumber++;
			   continue;
		   }
		   sorter.processRecord(line);
		   lineNumber++;
		}
		logger.info(String.format("Finished the import, parsed %d records", lineNumber - 1));
		br.close();
		return sorter;
	}
}
