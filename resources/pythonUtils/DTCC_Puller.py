import os
from urllib2 import urlopen, URLError, HTTPError
import calendar
import datetime

################################################################################################
#######     Downloads the cumulative rate files from DTCC     ##################################
################################################################################################

# Run this and it will create a dir DTCC with all the files in it

# update this every time you run the script
LAST_DOWNLOAD_YEAR = 2014
LAST_DOWNLOAD_MONTH = 8
LAST_DOWNLOAD_DAY = 28

DTCC_ROOT_DIR = "DTCC"

def dlfile(year, month, day):
    # Make the root dir if not exists
    try:
        os.stat(DTCC)
    except:
        os.mkdir(DTCC)
        
    # Make the year dir if not exists
    try:
        os.stat(year)
    except:
        os.mkdir(year)

    # Make the month dir if not exists
    try:
        os.stat(os.path.join(DTCC_ROOT_DIR, year, month))
    except:
        os.mkdir(os.path.join(year, month))

    # Open the url
    try:
        url = "https://kgc0418-tdw-data-0.s3.amazonaws.com/slices/CUMULATIVE_RATES_%s_%s_%s.zip" % (year, month.zfill(2), day.zfill(2))
        print "downloading " + url
        f = urlopen(url)

        # Open our local file for writing
        with open(os.path.join(year, month, os.path.basename(url)), "wb") as local_file:
            local_file.write(f.read())

    #handle errors
    except HTTPError, e:
        print "HTTP Error:", e.code, url
    except URLError, e:
        print "URL Error:", e.reason, url

#download the files from 2013 till now
def main():
    today = datetime.date.today()
    for year in range(2013, today.year + 1):
        for month in range(1, 13):
            c = calendar.monthcalendar(year, month)
            for week in c:
                for day in week:
                    if day != 0:
                        dlfile(str(year), str(month), str(day))

#Download all the files from last time you ran this file till today
def last_dl_to_today():
    today = datetime.date.today()
    last_download = datetime.date(LAST_DOWNLOAD_YEAR, LAST_DOWNLOAD_MONTH, LAST_DOWNLOAD_DAY)
    for year in range(LAST_DOWNLOAD_YEAR, today.year + 1):
        for month in range (1, 13):
            c = calendar.monthcalendar(year, month)
            for week in c:
                for day in week:
                    if day != 0:
                        print "Trying to parse %d %d %d" % (year, month, day)
                        parsing_date = datetime.date(year, month,day)
                        if parsing_date > last_download and parsing_date < today:
                            dlfile(str(year), str(month), str(day))

if __name__ == '__main__':
    #main()
    last_dl_to_today()